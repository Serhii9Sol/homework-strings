package main.java.strings;

import java.util.regex.Matcher;

public class TaskThree {

    public int getLengthOfShortestWord(String line){
        if (line == null){
            return -1;
        }
        int length = Integer.MAX_VALUE;
        String[] words = line.trim().split("[\\s\\,\\.\\!\\?\\-:;']+");
//        String[] words = line.trim().split("\\W+");
        for (int i = 0; i < words.length; i++) {
            if((words[i].length() > 0) && (length > words[i].length())){
                length = words[i].length();
            }
//            System.out.println(words[i]);
        }
        if(length==Integer.MAX_VALUE){
            length = 0;
        }
        return length;
    }

    public void replaceLastThreeSymbolsInWord(String[] words, int length){
        if(words == null || length < 0){
            throw new IllegalArgumentException();
        }
        String buffer;

        for(int i = 0; i < words.length; i++){
            if(words[i] == null){
                throw new IllegalArgumentException();
            }
            if(words[i].length() == length){
                if(length > 2){
                    buffer = words[i].substring(0, length - 3) + "$$$";
                } else {
                    buffer = words[i].replaceAll("\\w","\\$");
                }
                words[i] = buffer;
            }
        }
    }

    public String addSpaces(String line){
        if(line == null){
            throw new IllegalArgumentException();
        }
        if(line.isEmpty()){
            return "";
        }
        String result = "";
        char[] charArrline = line.toCharArray();
        int length = charArrline.length;
       //для всіх символів, крім останнього
        for (int i = 0; i < length - 1; i++) {
            if(charArrline[i] == '.' || charArrline[i] == ',' || charArrline[i] == '-' || charArrline[i] == ';'
                    || charArrline[i] == ':' || charArrline[i] == '`' || charArrline[i] == '!' || charArrline[i] == '?'){
                if(charArrline[i + 1] != ' '){
                    result = result + charArrline[i] + " ";
                } else {
                    result = result + charArrline[i];
                }
            } else {
                result = result + charArrline[i];
            }
        }
        //для останнього символа
        if(charArrline[length - 1] == '.' || charArrline[length - 1] == ','
                || charArrline[length - 1] == '-' || charArrline[length - 1] == ';'
                || charArrline[length - 1] == ':' || charArrline[length - 1] == '`'
                || charArrline[length - 1] == '!' || charArrline[length - 1] == '?'){
            result = result + charArrline[length - 1] + " ";
        } else {
            result = result + charArrline[length - 1];
        }
        return result;
    }

    public String getStringWithUnicChars(String line){
        if(line == null){
            throw new IllegalArgumentException();
        }
        String result = "";
        char[] charArrline = line.toCharArray();
        for(int i = 0; i < charArrline.length; i++){
            if(!result.contains(String.valueOf(charArrline[i]))){
                result = result + charArrline[i];
            }
        }
        return result;
    }

    public int getCountOfWords(String line){
        if(line == null){
            return -1;
        }
        int count = 0;
        if(line.isBlank()){
            return count;
        }
        String[] words = line.trim().split("\\s+");
        for(int i = 0; i < words.length; i++){
            if(!words[i].matches(".*[^a-zA-Z]+.*")) {
                count++;
            }
        }
        return count;
    }

    public String cutSubstringFromLine(String line, int position, int length){
        if(line == null || position < 0 || length < 0 || line.length() < position){
            throw new IllegalArgumentException();
        }
        String result = line.substring(0, position);
        if(line.length() >= position + length){
            result = result + line.substring(position + length);
        }
        return result;
    }

    public String reverseLine(String line){
        if(line == null){
            throw new IllegalArgumentException();
        }
        char[] lineChar = line.toCharArray();
        int length = lineChar.length;
        char[] reverseChar = new char[length];
        for (int i = 0; i < length; i++) {
            reverseChar[i] = lineChar[length - 1 - i];
        }
        return String.valueOf(reverseChar);
    }

    public String deleteLastWord(String line){
        if(line == null){
            throw new IllegalArgumentException();
        }
        if(line.isBlank()){
            return line;
        }
        char[] lineChar = line.toCharArray();
        int index = -1;
        for (int i = lineChar.length - 1; i >= 0; i--) {
            if(lineChar[i] != ' ' && lineChar[i - 1] == ' '){
                index = i;
                break;
            }
        }
        if(index == -1){
            return "";
        }
        line = line.substring(0, index);
        if(line.isBlank()){
            return "";
        }
        return line;
    }
}
