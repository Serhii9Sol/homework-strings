package main.java.strings;

public class TasksOneAndTwo {

    public String getSymbols(int startInterval, int endInterval){
        String result = "";
        if(startInterval <= endInterval){
            for(int numberOfSymbol = startInterval; numberOfSymbol < endInterval + 1; numberOfSymbol++){
                result = result + " " + (char) numberOfSymbol;
            }
        } else {
            for(int numberOfSymbol = startInterval; numberOfSymbol >= endInterval; numberOfSymbol--) {
                result = result + " " + (char) numberOfSymbol;
            }
        }
        return result;
    }

    public String convertingIntegerNumberToString(long number){
        if(number < 0){
            return "Отрицательные числа к натуральным не относят";
        }
        return String.valueOf(number);
    }

    public String convertingRealNumberToString(double number){
        return String.valueOf(number);
    }

    public long convertingStringToIntegerNumber(String number){
        return Long.parseLong(number);
    }

    public double convertingStringToRealNumber(String number){
        return Double.parseDouble(number);
    }
}
