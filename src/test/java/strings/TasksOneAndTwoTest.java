package test.java.strings;

import main.java.strings.TasksOneAndTwo;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class TasksOneAndTwoTest {

    private final TasksOneAndTwo cut = new TasksOneAndTwo();

    static Arguments[] getSymbolsTestArgs(){
        return new Arguments[]{
                Arguments.arguments(" A B C D E F G H I J K L M N O P Q R S T U V W X Y Z", 65, 90),
                Arguments.arguments(" z y x w v u t s r q p o n m l k j i h g f e d c b a", 122, 97),
                Arguments.arguments(" а б в г д е ж з и й к л м н о п р с т у ф х ц ч ш щ ъ ы ь э ю я", 1072, 1103),
                Arguments.arguments(" ! \" # $ % & ' ( ) * + , - . / 0 1 2 3 4 5 6 7 8 9 : ; < = > ? @ A B C D E F G H I J K L M N O P Q R S T U V W X Y Z [ \\ ] ^ _ ` a b c d e f g h i j k l m n o p q r s t u v w x y z { | } ~ \u007F \u0080 \u0081 \u0082 \u0083 \u0084 \u0085 \u0086 \u0087 \u0088 \u0089 \u008A \u008B \u008C \u008D \u008E \u008F \u0090 \u0091 \u0092 \u0093 \u0094 \u0095 \u0096 \u0097 \u0098 \u0099 \u009A \u009B \u009C \u009D \u009E \u009F   ¡ ¢ £ ¤ ¥ ¦ § ¨ © ª « ¬ \u00AD ® ¯ ° ± ² ³ ´ µ ¶ · ¸ ¹ º » ¼ ½ ¾ ¿ À Á Â Ã Ä Å Æ Ç È É Ê Ë Ì Í Î Ï Ð Ñ Ò Ó Ô Õ Ö × Ø Ù Ú Û Ü Ý Þ ß à á â ã ä å æ ç è é ê ë ì í î ï ð ñ ò ó ô õ ö ÷ ø ù ú û ü ý þ ÿ", 33, 255)
        };
    }

    @ParameterizedTest
    @MethodSource("getSymbolsTestArgs")
    void getSymbolsTest(String expected, int startInterval, int endInterval) {
        String actual = cut.getSymbols(startInterval, endInterval);

        assertEquals(expected, actual);
    }

    static Arguments[] convertingIntegerNumberToStringTestArgs(){
        return new Arguments[]{
                Arguments.arguments("2", 2),
                Arguments.arguments("Отрицательные числа к натуральным не относят", -2),
        };
    }

    @ParameterizedTest
    @MethodSource("convertingIntegerNumberToStringTestArgs")
    void convertingIntegerNumberToStringTest(String expected, long number) {
        String actual = cut.convertingIntegerNumberToString(number);

        assertEquals(expected, actual);
    }

    static Arguments[] convertingStringToIntegerNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments(2, "2"),
                Arguments.arguments(-2, "-2"),
        };
    }

    @ParameterizedTest
    @MethodSource("convertingStringToIntegerNumberTestArgs")
    void convertingStringToIntegerNumberTest(long expected, String number) {
        long actual = cut.convertingStringToIntegerNumber(number);

        assertEquals(expected, actual);
    }

    static Arguments[] convertingStringToIntegerNumberTestWithExceptionArgs(){
        return new Arguments[]{
                Arguments.arguments(NumberFormatException.class, "2.0"),
                Arguments.arguments(NumberFormatException.class, "2 2"),
                Arguments.arguments(NumberFormatException.class, "--5"),
                Arguments.arguments(NumberFormatException.class, "++5"),
                Arguments.arguments(NumberFormatException.class, "5l"),
                Arguments.arguments(NumberFormatException.class, ""),
                Arguments.arguments(NumberFormatException.class, null),
        };
    }

    @ParameterizedTest
    @MethodSource("convertingStringToIntegerNumberTestWithExceptionArgs")
    void convertingStringToIntegerNumberTestWithException(Class expected, String number) {
        assertThrows(expected, () -> cut.convertingStringToIntegerNumber(number));
    }

    static Arguments[] convertingStringToRealNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments(2.2, "2.2"),
                Arguments.arguments(-2.2, "-2.2"),
                Arguments.arguments(2, "2"),
        };
}

    @ParameterizedTest
    @MethodSource("convertingStringToRealNumberTestArgs")
    void convertingStringToRealNumberTest(double expected, String number) {
        double actual = cut.convertingStringToRealNumber(number);

        assertEquals(expected, actual);
    }

    static Arguments[] convertingStringToRealNumberTestWithExceptionArgs(){
        return new Arguments[]{
                Arguments.arguments(NumberFormatException.class, "2..0"),
                Arguments.arguments(NumberFormatException.class, "2 2"),
                Arguments.arguments(NumberFormatException.class, "--5.0"),
                Arguments.arguments(NumberFormatException.class, "++5.1"),
                Arguments.arguments(NumberFormatException.class, ".3."),
                Arguments.arguments(NumberFormatException.class, ""),
                Arguments.arguments(NullPointerException.class, null),
        };
    }

    @ParameterizedTest
    @MethodSource("convertingStringToRealNumberTestWithExceptionArgs")
    void convertingStringToRealNumberTestWithException(Class expected, String number) {
        assertThrows(expected, () -> cut.convertingStringToRealNumber(number));
    }
}