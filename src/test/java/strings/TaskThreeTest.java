package test.java.strings;

import main.java.strings.TaskThree;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class TaskThreeTest {

    private final TaskThree cut = new TaskThree();

    static Arguments[] getLengthOfShortestWordTestArgs(){
        return new Arguments[]{
                Arguments.arguments("aaaaa  bdc ,  .. kkkk!kk  . ", 2),
                Arguments.arguments(" - aa-bbb,.! cccc", 2),
                Arguments.arguments("zzzz      . aa-bbb,.! cccc", 2),
                Arguments.arguments("", 0),
                Arguments.arguments("     ", 0),
                Arguments.arguments("  , !!! ?    ", 0),
                Arguments.arguments(null, -1),
        };
    }

    @ParameterizedTest
    @MethodSource("getLengthOfShortestWordTestArgs")
    void getLengthOfShortestWordTest(String line, int expected) {
        int actual = cut.getLengthOfShortestWord(line);

        assertEquals(expected, actual);
    }
//    ********************************************************
    static Arguments[] replaceLastThreeSymbolsInWordTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new String[]{"aa","aa$$$", "bbbbbbb"},new String[]{"aa","aaaaa", "bbbbbbb"}, 5),
                Arguments.arguments(new String[]{"aa","aaaaa", "bbbbbbb"},new String[]{"aa","aaaaa", "bbbbbbb"}, 4),
                Arguments.arguments(new String[]{"$$", "bbbbbbb"},new String[]{"aa", "bbbbbbb"}, 2),
                Arguments.arguments(new String[]{"$", "bbbbbbb","$","$","fd"},new String[]{"a", "bbbbbbb","s","c","fd"}, 1),
                Arguments.arguments(new String[]{"","", ""},new String[]{"","", ""}, 0)
        };
    }

    @ParameterizedTest
    @MethodSource("replaceLastThreeSymbolsInWordTestArgs")
    void replaceLastThreeSymbolsInWordTest(String[] expected, String[] actual, int lenght){
        cut.replaceLastThreeSymbolsInWord(actual, lenght);

        assertArrayEquals(expected, actual);
    }

    static Arguments[] replaceLastThreeSymbolsInWordTestWithExceptionArgs(){
        return new Arguments[]{
                Arguments.arguments(new String[]{"aa","aaaaa", "bbbbbbb"}, -1),
                Arguments.arguments(null, 2),
                Arguments.arguments(new String[]{"rrr","aaaaa", null, "bbbbbbb"}, 4)
        };
    }

    @ParameterizedTest
    @MethodSource("replaceLastThreeSymbolsInWordTestWithExceptionArgs")
    void replaceLastThreeSymbolsInWordTestWithException(String[] actual, int lenght){
        assertThrows(IllegalArgumentException.class, ()-> cut.replaceLastThreeSymbolsInWord(actual,lenght));
    }
//    ********************************************************
    static Arguments[] addSpacesTestArgs(){
        return new Arguments[]{
                Arguments.arguments("asdf, dddf! asda . ", "asdf, dddf!asda ."),
                Arguments.arguments(" ; asdf,  dddf! ? asda . ", " ;asdf,  dddf!?asda ."),
                Arguments.arguments(" ! !  . ", " !!  . "),
                Arguments.arguments("   ", "   "),
                Arguments.arguments("", ""),
        };
    }

    @ParameterizedTest
    @MethodSource("addSpacesTestArgs")
    void addSpacesTest(String expected, String line){
        String actual = cut.addSpaces(line);

        assertEquals(expected,actual);
    }

    static Arguments[] addSpacesTestWithExceptionArgs(){
        return new Arguments[]{null};
    }

    @ParameterizedTest
    @MethodSource("addSpacesTestWithExceptionArgs")
    void addSpacesTestWithException(String line){
        assertThrows(IllegalArgumentException.class, ()-> cut.addSpaces(line));
    }
//    ********************************************************

    static Arguments[] getStringWithUnicCharsTestArgs(){
        return new Arguments[]{
                Arguments.arguments("asdf rewq,.%()", "asdfa frewq,., %%%() "),
                Arguments.arguments(" ", "   "),
                Arguments.arguments("", ""),
        };
    }

    @ParameterizedTest
    @MethodSource("getStringWithUnicCharsTestArgs")
    void getStringWithUnicCharsTest(String expected, String line){
        String actual = cut.getStringWithUnicChars(line);

        assertEquals(expected,actual);
    }

    @Test
    void getStringWithUnicCharsTestWithException(){

        assertThrows(IllegalArgumentException.class, ()-> cut.getStringWithUnicChars(null));
    }
//    ********************************************************

    static Arguments[] getCountOfWordsTestArgs(){
        return new Arguments[]{
                Arguments.arguments( 2, "  asd  fgh "),
                Arguments.arguments( 1, "  as2d  fgh "),
                Arguments.arguments( 0, "  as2d  f!!gh !21: "),
                Arguments.arguments( 0, "  "),
                Arguments.arguments( 0, ""),
                Arguments.arguments( -1, null),
        };
    }

    @ParameterizedTest
    @MethodSource("getCountOfWordsTestArgs")
    void getCountOfWordsTest(int expected, String line){
        int actual = cut.getCountOfWords(line);

        assertEquals(expected, actual);
    }
//    ********************************************************

    static Arguments[] cutSubstringFromLineTestArgs(){
        return new Arguments[]{
                Arguments.arguments( "asghjkl", "asdfghjkl", 2, 2),
                Arguments.arguments( "hjkl", "asdfghjkl", 0, 5),
                Arguments.arguments( "asdfgh", "asdfghjkl", 6, 3),
                Arguments.arguments( "asdfgh", "asdfghjkl", 6, 5),

        };
    }

    @ParameterizedTest
    @MethodSource("cutSubstringFromLineTestArgs")
    void cutSubstringFromLineTest(String expected, String line, int position, int length){
        String actual = cut.cutSubstringFromLine(line, position, length);

        assertEquals(expected, actual);
    }

    static Arguments[] cutSubstringFromLineTestWithExceptionArgs(){
        return new Arguments[]{
                Arguments.arguments(null, 2, 2),
                Arguments.arguments("qwert", -2, 2),
                Arguments.arguments("qwert", 2, -2),
                Arguments.arguments("qwert", 6, 2),

        };
    }


    @ParameterizedTest
    @MethodSource("cutSubstringFromLineTestWithExceptionArgs")
    void cutSubstringFromLineTestWithException(String line, int position, int length){

        assertThrows(IllegalArgumentException.class, ()-> cut.cutSubstringFromLine(line, position, length));
    }
//    ********************************************************

    static Arguments[] reverseLineTestArgs(){
        return new Arguments[]{
                Arguments.arguments("trewq","qwert"),
                Arguments.arguments(" d-s  a "," a  s-d "),
                Arguments.arguments("0.5-","-5.0"),
                Arguments.arguments("    ","    "),
                Arguments.arguments("",""),

        };
    }

    @ParameterizedTest
    @MethodSource("reverseLineTestArgs")
    void reverseLineTest(String expected, String line){
        String actual = cut.reverseLine(line);

        assertEquals(expected, actual);
    }

    @Test
    void reverseLineTestWithException(){

        assertThrows(IllegalArgumentException.class, ()-> cut.reverseLine(null));
    }
//    ********************************************************

    static Arguments[] deleteLastWordTestArgs(){
        return new Arguments[]{
                Arguments.arguments("  qwert  ","  qwert  asdfg"),
                Arguments.arguments("","  qwert  "),
                Arguments.arguments("   ","   "),
                Arguments.arguments("",""),

        };
    }

    @ParameterizedTest
    @MethodSource("deleteLastWordTestArgs")
    void deleteLastWordTest(String expected, String line){
        String actual = cut.deleteLastWord(line);

        assertEquals(expected, actual);
    }

    @Test
    void deleteLastWordTestWithException(){

        assertThrows(IllegalArgumentException.class, ()-> cut.deleteLastWord(null));
    }
}